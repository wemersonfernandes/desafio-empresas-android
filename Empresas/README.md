# App-empresas

Desafio da empresa [ioasys](https://www.ioasys.com.br/) realizado em linguagem [Kotlin](https://kotlinlang.org/) e organizado na arquitetura MVP. 

# Bibliotecas

Para a criação deste aplicativo foram utilizadas as seguintes bibliotecas:

* [RecyclerView ](https://developer.android.com/guide/topics/ui/layout/recyclerview) - utilizada para reciclar itens de uma lista enviando-os a um layout pré-definido.
* [Coil](https://github.com/coil-kt/coil) - utilizada para fazer o carregamento e manipulação das imagens do App.
* [Gson](https://github.com/google/gson) - utilizada para fazer a tratativa dos dados em formato JSON retornados pela API.
* [Retrofit](https://square.github.io/retrofit/) - utilizada para trabalhar com requisições da API.
* [Hawk](https://github.com/orhanobut/hawk) - utilizado para armazenamento de chaves no Android.

# O que faria se tivesse mais tempo?

* Melhoraria a validação do usuário.
* Implementaria a função de pesquisa utilizando o componente SearchView.
* Adicionaria loading durante as requisições.

Compreendo a importância dos outros itens dados como bônus, mas diante dos meus conhecimentos requereria mais tempo para estudos e implementação dos mesmos.

# Como executar a aplicação?

Na tela inicial você deve informar: 
 * E-mail: testeapple@ioasys.com.br
 * Senha: 1231234

Na tela seguinte deve-se escolher uma das empresas listadas e clicar sobre a mesma. Na tela seguinte será possível obter as informações sobre a respectiva empresa escolhida. Caso queira consultar outras empresas basta apenas clicar em voltar e selecionar outra empresa da lista. 








