package br.com.desafio.empresas.ui.login

import br.com.desafio.empresas.network.Api
import com.orhanobut.hawk.Hawk
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val ACCESS_TOKEN = "access-token"
const val CLIENT = "client"
const val UID = "uid"

class LoginPresenterImpl(private val view: LoginView) : LoginPresenter {
    override fun signIn(email: String, password: String) {
        if (isValid(email, password)) {
            Api.enterpriseService.signin(email, password).enqueue(object : Callback<Unit> {
                override fun onFailure(call: Call<Unit>, t: Throwable) {
                    view.showErrorLogin("Erro inesperado. Tente novamente")
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>) {
                    if (response.isSuccessful && response.code() == 200) {
                        saveCredentials(response)
                        view.loginSuccess()
                    } else {
                        view.showErrorLogin("Login inválido")
                    }
                }
            })
        } else {
            view.showErrorLogin("Preencha todos os dados")
        }
    }

    fun saveCredentials(response: Response<Unit>) {
        val accessToken = response.headers().get(ACCESS_TOKEN)
        val client = response.headers().get(CLIENT)
        val uid = response.headers().get(UID)

        Hawk.put(ACCESS_TOKEN, accessToken)
        Hawk.put(CLIENT, client)
        Hawk.put(UID, uid)
    }

    private fun isValid(email: String, password: String): Boolean {
        return email.isNotEmpty() && password.isNotEmpty()
    }
}