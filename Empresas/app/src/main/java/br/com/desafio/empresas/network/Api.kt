package br.com.desafio.empresas.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Api {
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://empresas.ioasys.com.br/api/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val enterpriseService: EnterpriseService = retrofit.create(EnterpriseService::class.java)
}
