package br.com.desafio.empresas.ui.enterprise

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.desafio.empresas.R
import br.com.desafio.empresas.model.Enterprise
import coil.api.load
import kotlinx.android.synthetic.main.item_enterprise.view.*

class EnterpriseAdapter(
    private val enterprises: List<Enterprise>,
    private val onEnterpriseClickListener: (Enterprise) -> Unit
) : RecyclerView.Adapter<EnterpriseAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_enterprise, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return enterprises.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val enterprise = enterprises[position]
        holder.bindView(enterprise, onEnterpriseClickListener)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val enterpriseName = itemView.textEnterprise
        private val type = itemView.textType
        private val country = itemView.textLocation
        private val photo = itemView.imageEnterprise

        fun bindView(enterprise: Enterprise, onEnterpriseClickListener: (Enterprise) -> Unit) {
            enterpriseName.text = enterprise.name
            type.text = enterprise.enterpriseType.name
            country.text = enterprise.country
            photo.load("https://empresas.ioasys.com.br" + enterprise.photo) {
                crossfade(true)
                error(R.drawable.img_e_1_lista)
                placeholder(R.drawable.img_e_1_lista)
            }
            itemView.setOnClickListener { onEnterpriseClickListener.invoke(enterprise) }
        }
    }
}