package br.com.desafio.empresas.ui.enterprise

import br.com.desafio.empresas.model.EnterpriseResponse
import br.com.desafio.empresas.network.Api
import br.com.desafio.empresas.ui.login.ACCESS_TOKEN
import br.com.desafio.empresas.ui.login.CLIENT
import br.com.desafio.empresas.ui.login.UID
import com.orhanobut.hawk.Hawk
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterprisePresenterImpl(private val enterpriseView: EnterpriseView) : EnterprisePresenter {

    private val uid = Hawk.get<String>(UID)
    private val client = Hawk.get<String>(CLIENT)
    private val accessToken = Hawk.get<String>(ACCESS_TOKEN)

    override fun getEnteprises() {
        Api.enterpriseService.getEnterprises(uid, client, accessToken)
            .enqueue(object : Callback<EnterpriseResponse> {
                override fun onFailure(call: Call<EnterpriseResponse>, t: Throwable) {
                    enterpriseView.showErro()
                }

                override fun onResponse(
                    call: Call<EnterpriseResponse>,
                    response: Response<EnterpriseResponse>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.enterprises?.let { enterprises ->
                            enterpriseView.showEnterprises(enterprises)
                        }
                    } else {
                        enterpriseView.showErro()
                    }
                }
            })
    }
}