package br.com.desafio.empresas.model

class EnterpriseResponse(
    val enterprises: List<Enterprise>
)