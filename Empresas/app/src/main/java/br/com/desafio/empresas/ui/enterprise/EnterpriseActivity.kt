package br.com.desafio.empresas.ui.enterprise

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.desafio.empresas.R
import br.com.desafio.empresas.model.Enterprise
import br.com.desafio.empresas.ui.enterprisedetails.EXTRA_ENTERPRISE
import br.com.desafio.empresas.ui.enterprisedetails.EnterpriseDetailsActivity
import kotlinx.android.synthetic.main.activity_enterprise.*

class EnterpriseActivity : AppCompatActivity(), EnterpriseView {

    private lateinit var enterprisePresenter: EnterprisePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enterprise)

        enterprisePresenter = EnterprisePresenterImpl(this)
        enterprisePresenter.getEnteprises()

    }

    override fun showErro() {
        Toast.makeText(this, "Erro ao carregar as informações", Toast.LENGTH_LONG).show()
    }

    override fun showEnterprises(enterprises: List<Enterprise>) {
        recyclerViewEnterprises.adapter = EnterpriseAdapter(enterprises) { enterprise ->
            val intent = Intent(this, EnterpriseDetailsActivity::class.java)
            intent.putExtra(EXTRA_ENTERPRISE, enterprise)
            startActivity(intent)
        }
        val layoutManager = LinearLayoutManager(this)
        recyclerViewEnterprises.layoutManager = layoutManager
    }
}
