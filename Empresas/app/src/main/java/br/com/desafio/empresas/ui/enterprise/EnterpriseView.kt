package br.com.desafio.empresas.ui.enterprise

import br.com.desafio.empresas.model.Enterprise

interface EnterpriseView {
    fun showErro()
    fun showEnterprises(enterprises: List<Enterprise>)
}