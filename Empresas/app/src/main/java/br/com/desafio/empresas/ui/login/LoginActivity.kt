package br.com.desafio.empresas.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.desafio.empresas.R
import br.com.desafio.empresas.ui.enterprise.EnterpriseActivity
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {

    private lateinit var presenter: LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        Hawk.init(this).build()

        presenter = LoginPresenterImpl(this)
        buttonLogin.setOnClickListener {
            it.isEnabled = false
            presenter.signIn(editTextEmail.text.toString(), editTextPassword.text.toString())
        }
    }

    override fun showErrorLogin(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        buttonLogin.isEnabled = true
    }

    override fun loginSuccess() {
        val intent = Intent(this, EnterpriseActivity::class.java)
        startActivity(intent)
    }
}
