package br.com.desafio.empresas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Enterprise(
    @SerializedName("enterprise_name")
    val name: String,
    @SerializedName("enterprise_type")
    val enterpriseType : EnterpriseType,
    val description: String,
    val country: String,
    val photo: String
) : Parcelable
