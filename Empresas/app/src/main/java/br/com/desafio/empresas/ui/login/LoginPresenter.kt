package br.com.desafio.empresas.ui.login

interface LoginPresenter {

    fun signIn(email: String, password: String)
}