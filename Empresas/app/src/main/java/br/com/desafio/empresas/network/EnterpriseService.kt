package br.com.desafio.empresas.network

import br.com.desafio.empresas.model.EnterpriseResponse
import retrofit2.Call
import retrofit2.http.*

interface EnterpriseService {
    @FormUrlEncoded
    @POST("users/auth/sign_in")
    fun signin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<Unit>

    @GET("enterprises")
    fun getEnterprises(@Header("uid") uid: String,
                       @Header("client") client: String,
                       @Header("access-token") accessToken: String): Call<EnterpriseResponse>
}