package br.com.desafio.empresas.ui.enterprise

interface EnterprisePresenter {
    fun getEnteprises()
}