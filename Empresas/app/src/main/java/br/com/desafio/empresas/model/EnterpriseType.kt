package br.com.desafio.empresas.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class EnterpriseType(
    @SerializedName("enterprise_type_name")
    val name: String
): Parcelable