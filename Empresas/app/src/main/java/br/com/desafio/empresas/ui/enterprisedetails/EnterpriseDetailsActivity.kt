package br.com.desafio.empresas.ui.enterprisedetails

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.desafio.empresas.R
import br.com.desafio.empresas.model.Enterprise
import coil.api.load
import kotlinx.android.synthetic.main.activity_enterprise_details.*

const val EXTRA_ENTERPRISE = "EXTRA_ENTERPRISE"

class EnterpriseDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enterprise_details)
        setupToolbar()

        val enterprise: Enterprise = intent.getParcelableExtra(EXTRA_ENTERPRISE)
        setupLayoutDetails(enterprise)
    }

    private fun setupLayoutDetails(enterprise: Enterprise) {
        supportActionBar?.title = enterprise.name
        textInfo.text = enterprise.description
        imageDetail.load("https://empresas.ioasys.com.br" + enterprise.photo) {
            crossfade(true)
            error(R.drawable.img_e_1_lista)
            placeholder(R.drawable.img_e_1_lista)
        }
    }
    
    private fun setupToolbar(){
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
    }
}
