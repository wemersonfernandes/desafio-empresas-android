package br.com.desafio.empresas.ui.login

interface LoginView {
    fun showErrorLogin(message: String)
    fun loginSuccess()
}